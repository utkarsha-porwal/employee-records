package com.assignment.week2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class DataStructureB {
	
	public void cityNameCount(ArrayList<Employee> employees) {
		
		//Storing city into the ArrayList
		ArrayList<String> city=new ArrayList<String>();
		for(Employee emp : employees) {
			city.add(emp.getCity());
		}
		
		//Using TreeMap for printing in alphabetical order
		Map<String, Integer> counts = new TreeMap<String, Integer>();
		
		
		//Counting similar City names
		for (String str : city) {
		    if (counts.containsKey(str)) {
		        counts.put(str, counts.get(str) + 1);
		    } else {
		        counts.put(str, 1);
		    }
		}

		for (Map.Entry<String, Integer> entry : counts.entrySet()) {
		    counts.put(entry.getKey(),entry.getValue());
		}
		System.out.println(counts);	
	}
		
	
	//Method for calculating monthly salary from annual salary
	public void monthlySalary(ArrayList<Employee> employees) {
		
		//Using HashMap just for printing key value
		HashMap<Integer, Float> h1=new HashMap<Integer,Float>();
		for(Employee emp : employees) {
			float salary=emp.getSalary()/12;
			h1.put(emp.getId(), salary);
		}
		System.out.println(h1);
	}
}
