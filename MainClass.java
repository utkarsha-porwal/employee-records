package com.assignment.week2;


import java.util.ArrayList;
import java.text.NumberFormat;


public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

			ArrayList<Employee> employees=new ArrayList<Employee>();
			
			//Creating object of Employee Class
			Employee e1=new Employee();	
			e1.setId(1);
			e1.setName("Aman");
			e1.setAge(20);
			e1.setSalary(1100000);
			e1.setDepartment("IT");
			e1.setCity("Delhi");
			
			//Creating object of Employee Class
			Employee e2=new Employee();  
			e2.setId(2);
			e2.setName("Bobby");
			e2.setAge(22);
			e2.setSalary(500000);
			e2.setDepartment("HR");
			e2.setCity("Bombay");
			
			//Creating object of Employee Class
			Employee e3=new Employee();  
			e3.setId(3);
			e3.setName("Zoe");
			e3.setAge(20);
			e3.setSalary(750000);
			e3.setDepartment("Admin");
			e3.setCity("Delhi");
			
			//Creating object of Employee Class
			Employee e4=new Employee(); 
			e4.setId(4);
			e4.setName("Smitha");
			e4.setAge(21);
			e4.setSalary(1000000);
			e4.setDepartment("IT");
			e4.setCity("Chennai");
			
			//Creating object of Employee Class
			Employee e5=new Employee();  
			e5.setId(5);
			e5.setName("Smitha");
			e5.setAge(24);
			e5.setSalary(1200000);
			e5.setDepartment("HR");
			e5.setCity("Bengaluru");
			
			//adding each employee details 
			employees.add(e1);  
			employees.add(e2);
			employees.add(e3);
			employees.add(e4);
			employees.add(e5);
			
			for(Employee emp : employees) {
				//  Checking exceptions for Each Employee
				if(emp.getId()<0) {
					throw new IllegalArgumentException("Illegal Argument Exception");
				}if(emp.getName()==null) {
					throw new IllegalArgumentException("Illegal Argument Exception");
				}if(emp.getAge()<0) {
					throw new IllegalArgumentException("Illegal Argument Exception");
				}if(emp.getSalary()<0) {
					throw new IllegalArgumentException("Illegal Argument Exception");
				}if(emp.getDepartment()==null) {
					throw new IllegalArgumentException("Illegal Argument Exception");
				}if(emp.getCity()==null) {
					throw new IllegalArgumentException("Illegal Argument Exception");
				}
			}
			
			//details of each Employees
			System.out.println("Sr No  Name  Age  Salay(INR)  Department Location");
			for(Employee emp:employees) {
				String formattedSalary=NumberFormat.getInstance().format(emp.getSalary()); 
				System.out.println(emp.getId()+"  "+emp.getName()+"  "+emp.getAge()+"  "+formattedSalary+"  "+emp.getDepartment()+"  "+emp.getCity());
			}
			
			System.out.println("\n");
			
			//Sorting Employee's Name
			DataStructureA dsA=new DataStructureA();
			System.out.println("Names of all employees in the sorted order are : ");
			dsA.sortingNames(employees);
			
			System.out.println("\n");
			
			//Counting Employees from each city
			System.out.println("Count of Employees from each city:");
			DataStructureB dsB=new DataStructureB();
			dsB.cityNameCount(employees);
			
			System.out.println("\n");
			
			//Calculating Monthly salary of each employee
			System.out.println("Monthly Salary of employee along with their ID is:");
			dsB.monthlySalary(employees);

	}

}
